'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
	.directive('headerNotification', function() {
		return {
			templateUrl: 'header/header-notification/view.header-notification.html',
			restrict: 'E',
			replace: true,
		}
	});
