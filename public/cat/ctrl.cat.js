'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
   .controller('CatCtrl', function($scope, server) {
      var vm = $scope;
      vm.submit = function() {
         console.log('vm.docForm.data', vm.docForm.data);
         server.upload('', vm.docForm, function(e, r) {
            if (!e) {
               console.log('saved: \'doc\'');
            }
         });
      };

   });
