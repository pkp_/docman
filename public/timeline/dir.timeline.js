'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
	.directive('timeline', function() {
		return {
			templateUrl: 'timeline/view.timeline.html',
			restrict: 'E',
			replace: true,
		}
	});
