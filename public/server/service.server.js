'use strict';

angular.module('sbAdminApp')
   .factory('server', function($http, $state, $window, LoginService) {
      var ginfo = LoginService;
      return {

         crud: function(method, path, data, cb) {
            var req = {
               method: method, //'GET'
               url: ginfo.domain + '/' + path,
               headers: {
                  'Content-Type': 'application/json'
               },
               withCredentials: true
            };
            req.data = data;
            $http(req)
               .success(function(response) {
                  cb(null, response.result);
               })
               .error(function(response, status) {
                  if (status === 401) {
                     return $state.go('login');
                  }
                  if (response.code == 11000) {}
               });
         },
         upload: function(path, data, cb) {
            var req = {
               method: 'POST',
               url: ginfo.domain + '/' + path,
               headers: {
                  'Content-Type': undefined
               },
               data: data,
               withCredentials: true,
               transformRequest: function(data1) {
                  return data1;
               }
            };
            $http(req)
               .success(function(response) {
                  cb(null, response.result);
               }).error(function(response, status) {
                  if (status === 401) {
                     return $state.go('login');
                  }
               });
         },
         download: function(method, path, data, cb) {
            var req = {
               url: ginfo.domain + '/' + path,
               method: 'POST',
               data: data,
               headers: {
                  'Content-Type': 'application/json'
               },
               responseType: 'arraybuffer',
               withCredentials: true
            };

            $http(req)
               .success(function(data, status, headers, config) {
                  // return console.log(config);
                  var fname = config.url.slice(-1) == 'f' ? '.pdf' : '.xlsx';
                  var file = new Blob([data]);
                  //trick to download store a file having its URL
                  var fileURL = URL.createObjectURL(file);
                  var a = document.createElement('a');
                  a.href = fileURL;
                  a.target = '_blank';
                  a.download = 'yourfilename' + fname;
                  document.body.appendChild(a);
                  a.click();
                  cb(null, {});
               }).error(function(response, status) {
                  if (status === 401) {
                     return $state.go('login');
                  }
               });
         }

      };

   });
