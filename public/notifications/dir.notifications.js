'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
	.directive('notifications', function() {
		return {
			templateUrl: 'notifications/view.notifications.html',
			restrict: 'E',
			replace: true,
		}
	});
