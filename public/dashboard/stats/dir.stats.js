'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
   .directive('stats', function() {
      return {
         templateUrl: 'dashboard/stats/view.stats.html',
         restrict: 'E',
         replace: true,
         scope: {
            'model': '=',
            'comments': '@',
            'number': '@',
            'name': '@',
            'colour': '@',
            'details': '@',
            'type': '@',
            'goto': '@'
         }

      }
   });
