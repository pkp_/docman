'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */

angular.module('sbAdminApp')
   .directive('sidebar', ['$location', function() {
      return {
         templateUrl: 'sidebar/view.sidebar.html',
         restrict: 'E',
         replace: true,
         scope: {},
         controller: function($scope) {
            $scope.selectedMenu = 'dashboard';
            $scope.collapseVar = 0;
            $scope.multiCollapseVar = 0;

            $scope.check = function(x) {

               if (x == $scope.collapseVar)
                  $scope.collapseVar = 0;
               else
                  $scope.collapseVar = x;
            };

            $scope.multiCheck = function(y) {

               if (y == $scope.multiCollapseVar)
                  $scope.multiCollapseVar = 0;
               else
                  $scope.multiCollapseVar = y;
            };

            IsSelectable($scope);

         }
      };
   }]);

function IsSelectable($scope) {
   $scope.treedata = [{
         label: "Documents",
         type: "folder",
         children: [{
            label: "a picture",
            type: "pic"
         }, {
            label: "another picture",
            type: "pic"
         }, {
            label: "a doc",
            type: "doc"
         }, {
            label: "a file",
            type: "file"
         }, {
            label: "a movie",
            type: "movie"
         }]
      }, {
         label: "My Computer",
         type: "folder",
         children: [{
            label: "email",
            type: "email"
         }, {
            label: "home",
            type: "home"
         }]
      }, {
         label: "trash",
         type: "trash"
      }

   ];

   // $scope.opts = {
   //    isSelectable: function(node) {
   //       return node.label.indexOf("Node 1") !== 0;
   //    }
   // };
   $scope.showSelected = function(sel) {
      $scope.selectedNode = sel;
   };
}
