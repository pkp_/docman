'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
   .controller('LoginCtrl', function($scope, $state, LoginService) {

      $scope.submit = function(u, p) {

         if (u == 'admin' && p == 'admin') {
            LoginService.setUser({
               name: 'test',
               role: 'admin'
            });
            return $state.go('dashboard.home');
         }
      };

   });
