var db_name = 'docmanager';
var mongodb_connection_string = 'mongodb://127.0.0.1:27017/' + db_name;
if (process.env.OPENSHIFT_NODEJS_IP) {
   var mongodb_connection_string = 'mongodb://pkp:$#Complex#$@ds115738.mlab.com:15738/docmanager';
}
module.exports = {
   'mongo_url': mongodb_connection_string,
   'super_user': {
      'code': 'super_user_1',
      'name': 'super',
      'username': 'super',
      'password': 'super123',
      'role': 'super'
   }
};
