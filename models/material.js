var mongoose = require('mongoose');
var Schema = mongoose.Schema,
   ObjectId = Schema.ObjectId;

var Material = new Schema({
   name: {
      type: String,
      required: true
   },
   poNumber1: {
      type: String,
      required: true,
      unique: false,
      lowercase: true
   },
   poNumber: {
      type: String,
      required: true
   },
   amount: {
      type: Number,
      required: true
   },
   materialType: {
      type: ObjectId,
      ref: 'MaterialType',
      required: true
   },
   supplier: {
      type: ObjectId,
      ref: 'Supplier',
      required: true
   },
   job: {
      type: ObjectId,
      ref: 'Job',
      required: true
   },
   branch: {
      type: ObjectId,
      ref: 'Branch'
   },
   created: {
      type: Date
   },
   updated: {
      type: Date
   },
   updatedby: {
      type: ObjectId,
      ref: 'User'
   },
   createdby: {
      type: ObjectId,
      ref: 'User'
   },
   status: {
      type: ObjectId,
      ref: 'Status'
   }
});

if (!Material.options.toJSON) Material.options.toJSON = {};
Material.options.toJSON.transform = function(doc, ret, options) {
   // remove the _id of every document before returning the result
   delete ret.__v;
   return ret;
};


module.exports = mongoose.model('Material', Material);
