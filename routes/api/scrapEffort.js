var express = require('express');
var router = express.Router();
var SubCategory = require('../../controllers').subCategory;
var Middleware = require('./middleware');

router.get('/', Middleware.isAuthenticated, function(req, res, next) {
   SubCategory.findAll(req, res, function(err, scrapEffort) {
      if (err) {
         console.log(err);
         return res.status(500).send(err);
      }
      return res.status(200).send({
         result: scrapEffort,
         status: 200
      });
   });
});

router.post('/save', Middleware.isAuthenticated, function(req, res, next) {
   var save = req.body._id ? SubCategory.update : SubCategory.create;
   save(req, res, function(err, dat) {
      if (err) {
         console.log(err);
         return res.status(500).send({
            message: err.message,
            status: 500
         });
      }
      return res.status(200).send({
         message: 'SubCategory has been saved',
         status: 200
      });
   });
});

router.post('/delete', Middleware.isAuthenticated, function(req, res, next) {
   SubCategory.delete(req, res, function(err, dat) {
      if (err) {
         console.log(err);
         return res.status(500).send(err);
      }
      return res.status(200).send({
         message: 'SubCategory has been deleted',
         status: 200
      });
   });
});


router.post('/jobScrapEffort', Middleware.isAuthenticated, function(req, res, next) {
   SubCategory.jobScrapEffort(req, res, function(err, dat) {
      if (err) {
         console.log(err);
         return res.status(500).send(err);
      }
      return res.status(200).send({
         result: dat,
         status: 200
      });
   });
});


router.post('/update', Middleware.isAuthenticated, function(req, res, next) {
   SubCategory.update(req, res, function(err, dat) {
      if (err) {
         console.log(err);
         return res.status(500).send({
            message: err.message,
            status: 500
         });
      }
      return res.status(200).send({
         message: 'Updated',
         status: 200
      });
   });
});


module.exports = router;
