var express = require('express');
var router = express.Router();
var Category = require('../../controllers').category;
var Middleware = require('./middleware');

router.get('/', Middleware.isAuthenticated, Middleware.isAdminUser, function(req, res, next) {
   Category.findAll(req, res, function(err, suppliers) {
      if (err) {
         return res.status(500).send(err);
      }
      return res.status(200).send({
         result: suppliers,
         status: 200
      });
   });
});

router.post('/save', Middleware.isAuthenticated, Middleware.isAdminUser, function(req, res, next) {
   var save = req.body._id ? Category.update : Category.create;
   save(req, res, function(err, dat) {
      if (err) {
         return res.status(500).send(err);
      }
      return res.status(200).send({
         message: 'Category has been saved',
         status: 200
      });
   });
});

router.post('/delete', Middleware.isAuthenticated, Middleware.isAdminUser, function(req, res, next) {
   Category.delete(req, res, function(err, dat) {
      if (err) {
         return res.status(500).send(err);
      }
      return res.status(200).send({
         message: 'Category has been deleted',
         status: 200
      });
   });
});



router.get('/list', Middleware.isAuthenticated, Middleware.isAdminUser, function(req, res, next) {
   Category.list(req, res, function(err, dat) {
      if (err) {
         console.log(err);
         return res.status(500).send(err);
      }
      return res.status(200).send({
         result: dat,
         status: 200
      });
   });
});


module.exports = router;
