var express = require('express');
var router = express.Router();
var passport = require('passport');
var Middleware = require('./middleware');
var Category = require('../controllers').category;
var SubCategory = require('../controllers').subCategory;
var User = require('../controllers').user;

router.get('/login', function(req, res) {
   res.render('login', {
      user: req.user
   });
});

router.post('/home', passport.authenticate('local'), function(req, res) {
   console.log(req.body);
   res.render('home');
});

router.get('/logout', function(req, res) {
   req.logout();
});

router.get('/', Middleware.isAuthenticated, function(req, res) {
   res.render('home');
});



module.exports = router;
